# Part of LunarLander
# by Rene Kjellerup 2021
# published under the GNU GPL 3.0 or later (see LICENSE file)
extends MarginContainer

@export var velocity := 0.0 
@export var touch_down := 0.0
@export var fuel := 0.0
@export var exploded := false
@export var landed := false
@export var in_delay := 0.0
# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"
@onready var t = find_child("GUI_TextOut")
@onready var l = find_child("lander")
@onready var gv = find_child("velocity_vec")
# Called when the node enters the scene tree for the first time.
func _process(_delta: float) -> void:
	if not exploded and not landed:
		t.text = "Velocity: {0}m/s\nFuel: {1}\nCurrent Delay:{2}".format(["%0.3f" % velocity, "%0.3f" % fuel, "%0.2f" % in_delay])
	elif not exploded:
		t.text = "Success you have landed\nRemaining Fuel: {0}\npress 'R' or 'F5' to go again".format(["%0.3f" % fuel])
	else:
		t.text = "boom, you've crashed\nwith the Velocity of {0}m/s\npress 'R' or 'F5' to try again".format(["%0.3f" % touch_down])

func set_dead(v) -> void:
	exploded = true
	touch_down = v

func set_td() -> void:
	landed = true

func update_ui(v, f, r) -> void:
	velocity = v.length() / 100
	fuel = f
	l.rotation = r
	v.y *= -1
	gv.rotation = v.angle_to(Vector2(0,-10))
	if v.length() > 0.0:
		landed = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass
