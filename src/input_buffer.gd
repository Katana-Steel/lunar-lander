# Part of LunarLander
# by Rene Kjellerup 2021
# published under the GNU GPL 3.0 or later (see LICENSE file)
extends Control

signal input_burn(d, b)
signal input_rotate(de, di)
var ccw :bool =false
var cw :bool= false
var up :bool = false 
@export var com_delay = 1.5
var internal_delay = 1.5

# Called when the node enters the scene tree for the first time.
func _ready():
	if not (OS.has_feature("mobile") or OS.has_feature("web")):
		find_child("CCW").visible = false
		find_child("CW").visible = false
		find_child("Burn").visible = false
	internal_delay = com_delay

func round_trip_burn(delta, burn):
	if com_delay > 0.0:
		await get_tree().create_timer(com_delay).timeout
	emit_signal("input_burn", delta, burn)

func round_trip_rotate(delta, dir):
	if com_delay > 0.0:
		await get_tree().create_timer(com_delay).timeout
	emit_signal("input_rotate", delta, dir)

func step_delay(delta, dir):
	internal_delay += delta * dir
	if internal_delay - com_delay > 0.25:
		com_delay += 0.25
		internal_delay = com_delay
	if internal_delay - com_delay < -0.25:
		com_delay -= 0.25
		internal_delay = com_delay
	if com_delay < 0.0:
		internal_delay = 0.0
		com_delay = 0.0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var burn = Input.is_action_pressed("burn") or up
	var rotate = Input.get_action_strength('rotate_cw') - Input.get_action_strength('rotate_ccw')
	rotate += (1 if cw else 0) -  (1 if ccw else 0)
	rotate = clamp(rotate, -1.0, 1.0)
	var delay_update = Input.get_action_strength("delay_step_up") - Input.get_action_strength("delay_step_down")
	if burn:
		var power = Input.get_action_strength("burn") + (1.0 if up else 0.0)
		round_trip_burn(delta, clamp(power, 0.0, 1.0))
	if rotate != 0:
		round_trip_rotate(delta, rotate)
	if delay_update != 0:
		step_delay(delta, delay_update)


func _on_burn_toggled(button_pressed):
	self.up = button_pressed

func _on_ccw_toggled(button_pressed):
	self.ccw = button_pressed

func _on_cw_toggled(button_pressed):
	self.cw = button_pressed
