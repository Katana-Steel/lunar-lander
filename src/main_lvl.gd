# Part of LunarLander
# by Rene Kjellerup 2021
# published under the GNU GPL 3.0 or later (see LICENSE file)
extends Node2D

@onready var pl = $Player
@onready var gui = $ParallaxBackground/GUI
@onready var tm = $TileMap
@onready var ts = tm.tile_set
@onready var inp = $input_buffer
var _rng = FastNoiseLite.new()
var max_floor = 80
var min_floor = -15
var max_x = 18000

func _ready() -> void:
	_rng.noise_type  = FastNoiseLite.TYPE_SIMPLEX
	_rng.seed        = randi()
	for i in range(min_floor, max_floor):
		drawFloor(i)


func drawFloor(x):
	var rg = absi(_rng.get_noise_2d(x, 0) * 8) + 26
	for c in range(0, rg):
		set_cell(x, c, -1)
	for c in range(rg, 50):
		set_cell(x, c,  1)

	var left = get_cell(x-1, rg)
	if left == -1: # left edge detection
		set_cell(x, rg, 0)
		return
	# right edge detectoion
	left = get_cell(x-1, rg-1)
	if left == 1:
		while left != -1:
			rg -= 1
			left = get_cell(x-1, rg-1)
		left = get_cell(x-1, rg)
		if left == 1:
			set_cell(x-1, rg, 2)


func _quit():
	get_tree().quit(0)

func _wrap_around():
	var cell = floor((pl.position.x / tm.tile_set.tile_size.x))
	if cell > (max_floor-7):
		for i in range(max_floor, max_floor+10):
			drawFloor(i)
		max_floor += 10
	if cell < (min_floor+5):
		for i in range(min_floor-20, min_floor+1):
			drawFloor(i)
		min_floor -= 20
	#while pl.position.x > max_x:
	#	pl.position.x -= max_x
	#while pl.position.x < 0:
	#	pl.position.x += max_x

func _process(_delta):
	if Input.get_action_strength('quit') > 0.0:
		_quit()
	if Input.get_action_strength('reload') > 0.0:
		_restart()
	if get_node_or_null("Player"):
		_wrap_around()
	gui.in_delay = inp.com_delay

func _restart():
	var r = get_tree().root
	var menu = ResourceLoader.load("res://main_menu.tscn").instantiate()
	r.remove_child(self)
	r.add_child(menu)

# tile map helper functions (3.x style)
func get_cell(x, y) -> int:
	var source_id = tm.get_cell_source_id(0, Vector2i(x, y))
	if ts.get_source_id(0) == source_id:
		return 0
	if ts.get_source_id(1) == source_id:
		return 1
	return -1

func set_cell(x, y, t) -> void:
	var source_id = ts.get_source_id(t)
	tm.set_cell(0, Vector2(x, y), source_id, Vector2.ZERO)
	
