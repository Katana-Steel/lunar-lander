# Part of LunarLander
# by Rene Kjellerup 2021
# published under the GNU GPL 3.0 or later (see LICENSE file)
extends CharacterBody2D

@export var gravity: = 162.50  # 1.625 m/(s*s) constant luna gravity
@export var up: = Vector2.UP
@export var burn_boost: = 15
@export var rot_boost: = 1
@export var fuel: = 10.0
@export var round_trip_delay: = 2.5
var is_burning = false
var has_landed = false

signal exploded(vel)
signal moved(vel, fuel, rot)
signal landed

func _ready():
	up = up.rotated(rotation).normalized()
	velocity = Vector2(350.0, 0.5)

func _process(delta: float) -> void:
	if $AnimationPlayer.current_animation == 'explosion':
		if not $thrust.playing or $thrust.get_playback_position() > 0.2:
			$thrust.pitch_scale = 0.88
			$thrust.volume_db = -2.3
			$thrust.play(delta)
		velocity = Vector2.ZERO
		$AnimationPlayer.advance(delta)
		await $AnimationPlayer.animation_finished
		queue_free()
		return
	
	if is_burning:
		$AnimationPlayer.play('thrust')
		if not $thrust.playing or $thrust.get_playback_position() < 0.3:
			$thrust.play(0.31)
		is_burning = false
	else:
		$thrust.stop()
		$AnimationPlayer.play('Idle')

func _physics_process(delta: float) -> void:
	velocity.y += gravity * delta
	if $AnimationPlayer.current_animation == 'explosion':
		velocity = Vector2.ZERO
		return
	if is_on_floor():
		velocity = Vector2.ZERO
	# rotate_body(delta)
	# velocity = add_burn(delta, velocity)
	move(delta)
	if not has_landed:
		moved.emit(velocity, fuel, rotation)


func move(delta: float) -> void:
	var collide = move_and_collide(velocity * delta)
	if collide and velocity.length() > 260:
		explode()
	elif collide and velocity.length() > 30:
		velocity = Vector2.UP * (velocity.length()/2) * delta
	elif collide:
		velocity = Vector2.ZERO
		touch_down()


func explode() -> void:
	exploded.emit(velocity.length()/100)
	$AnimationPlayer.play('explosion')


func touch_down() -> void:
	has_landed = true
	landed.emit()


func add_burn(delta: float, vel: Vector2) -> Vector2:
	var strength: = 0.0
	if (fuel > 0.0):
		has_landed = false
		is_burning = true
		strength = delta * Input.get_action_strength('burn')
		fuel -= strength
	return vel + (up * (strength*burn_boost))

func do_burn(delta: float, burn) -> void:
	var strength: = 0.0
	if (fuel > 0.0):
		has_landed = false
		is_burning = true
		strength = delta * burn
		fuel -= strength
	velocity += (up * (strength*burn_boost))

func do_rotate(delta: float, dir: int) -> void:
	var rot = dir * rot_boost * delta
	up = up.rotated(rot).normalized()
	rotation += rot

func rotate_body(delta: float) -> void:
	var dir = Input.get_action_strength('rotate_cw') - Input.get_action_strength('rotate_ccw')
	var rot = dir * rot_boost * delta
	up = up.rotated(rot).normalized()
	rotation += rot
