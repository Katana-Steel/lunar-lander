# Part of LunarLander
# by Rene Kjellerup 2021
# published under the GNU GPL 3.0 or later (see LICENSE file)
extends CharacterBody2D
class_name Actor

@export var gravity: = 162.50  # 1.625 m/(s*s) constant luna gravity
@export var velocity: = Vector2.ZERO


func _physics_process(delta: float) -> void:
	velocity.y += gravity * delta


