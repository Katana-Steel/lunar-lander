# Part of LunarLander
# by Rene Kjellerup 2021
# published under the GNU GPL 3.0 or later (see LICENSE file)
extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


func load_level(delay):
	var lvl = ResourceLoader.load("res://main_lvl.tscn").instantiate()
	var buff = lvl.find_child("input_buffer")
	buff.com_delay = delay
	var root = get_tree().root
	root.remove_child(self)
	root.add_child(lvl)


func load_level_lem():
	load_level(0.0)
	
func load_level_orbit():
	load_level(0.5)
	
func load_level_earth():
	load_level(2.5)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
