# Lunar Lander 

this is written using [Godot 3.3](https://godotengine.org/)

The backgound is adapted from NASA's picture "[Earthrise from Apollo 8](https://moon.nasa.gov/resources/114/the-rising-earth-as-seen-by-apollo-8/)"

all sprite/image assest were created with [Gimp 2.10](https://www.gimp.org/)
